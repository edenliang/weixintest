
Page({
  buy: function (event) {
    let that = this;
    let dataset = event.target.dataset;
    if (dataset.price <= this.data.total) {
      let list = [...this.data.list, dataset];
      setInterval(function () { that.work(dataset) }, dataset.time);
      that.setData({
        total: this.data.total - dataset.price,
        list: list
      })
    }
  },
  work: function (obj) {
     this.setData({
        total: this.data.total + parseInt(obj.val),
      })
  },
  data: {
    content: 'test',
    total: 200,
    list: [],
    workList: [{ name: "pp", val: 1, time: 5000, price: 10 },
    { name: "阿绿", val: 2, time: 5000, price: 20 },
    { name: "wesin", val: 2, time: 2000, price: 40 },
    { name: "eden", val: 2, time: 1000, price: 80 }]
  }
})
